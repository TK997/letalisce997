<div class="container" id="klobasa">

    <?php echo form_open('clanstvo/dodajClana', ['class' => 'horizontal-form']); ?>
    
    
    <h3>Dodaj Clana</h3>
    <div style="padding-left: 320px">

        <input type="submit" class="btn btn-outline-success" href="<?php echo base_url(); ?>index.php/clanstvo/dodajClana" value="Dodaj" />

    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Ime</label>
                <div class="col-md-9">

                    <?php echo form_input(['name' => 'ime', 'class' => 'form-control', 'placeholder' => 'Ime']); ?>
                    <?php echo form_error('ime'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Priimek</label>
                <div class="col-md-9">

                    <?php echo form_input(['name' => 'priimek', 'class' => 'form-control', 'placeholder' => 'Priimek']); ?>
                    <?php echo form_error('priimek'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Email</label>
                <div class="col-lg-9" name="email">

                    <?php echo form_input(['name' => 'email', 'class' => 'form-control', 'placeholder' => 'ime.priimek@xmail.com']); ?>
                    <?php echo form_error('email'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Geslo</label>
                <div class="col-lg-9">

                    <?php echo form_input(['name' => 'geslo', 'type' => 'password', 'class' => 'form-control', 'placeholder' => 'Geslo']); ?>
                    <?php echo form_error('geslo'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label class="col-md-5 control-label">Geslo ponovno</label>
                <div class="col-lg-9">

                    <?php echo form_input(['name' => 'geslopon', 'type' => 'password', 'class' => 'form-control', 'placeholder' => 'Vtipkaj geslo ponovno']); ?>
                    <?php echo form_error('geslopon'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group" id="izbira">
                <label class="col-md-3 control-label">Spol</label>
                <select class="col-lg-9" name="spol">
                    <option value="">Izberi</option>
                    <option value="Moški">Moški</option>
                    <option value="Ženska">Ženska</option>
                </select>
                <?php echo form_error('spol'); ?>
            </div>
        </div>
        <div class="col-md-6">

        </div>

    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group" id="izbira">
                <label class="col-md-3 control-label">Rank</label>
                <select class="col-lg-9" name="rank">
                    
                    <option disabled value="Admin">Admin</option>
                    <option disabled value="Zaposleni">Zaposleni</option>
                    <option value="Član">Član</option>
                </select>
                <?php echo form_error('rank'); ?>
            </div>
        </div>
        <div class="col-md-6">

        </div>

    </div>
    <?php echo form_close(); ?>

</div>