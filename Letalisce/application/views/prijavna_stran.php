<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Prijavi se</title>
    <meta charset="utf-8" content="width=device-width, initial-scale=5">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/global.css');?>">
    
  </head>
  <body class="ozadje">
 
      <div class="container">
        <!-- center_div je css class, ki postima form na sredino-->
       <div class="col-md-6 col-md-offset-4 center_div" >

       <!-- form od tle naprej-->
         <form class="form-signin ozadje_form" action="<?php echo site_url('prijava/avtentikacija_podatkov');?>" method="post">
           
            <div class="form-group center_button">
                <h2 class="form-signin-heading center_button" >Prijavi se</h2>
                <!-- Pokazi da so podatki napacni-->
                <?php echo $this->session->flashdata('napaka');?>
                <label for="ime" class="sr-only">Ime</label>
                <input type="email" name="email" class="form-control" placeholder="Email" required autofocus>
            </div>    

           
            <div class="form-group">
                <label for="geslo" class="sr-only">Geslo</label>
                <input type="password" name="geslo" class="form-control" placeholder="Geslo" required>
            </div>
           
           <div class="checkbox center_button">
             <label>
               <input type="checkbox" value="remember-me"> Zapomni si me
             </label>
           </div>
           <button class="btn btn-lg btn-success btn-block" type="submit">Prijava</button>
         </form>
        </div>
       </div> <!-- /container -->
 
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
  </body>
</html>