<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Pozdravljeni</title>
    <meta charset="utf-8" content="width=device-width, initial-scale=5">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
    
    
  </head>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="first-slide" src="<?php echo base_url('assets/slike/letalisce_prijava.jpg'); ?>" alt="First slide">
            <div class="container">
              <div class="carousel-caption text-left">
                <h1>Letite z najboljšimi letali!</h1>
                <p>Letala so VSAKODNEVNO preverjena zaradi vaše <span style="color:white">VARNOSTI</span> in <span style="color:white">KOMFORTA</span>.</p>
                
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <img class="second-slide" src="<?php echo base_url('assets/slike/Alpe.png'); ?>" alt="Second slide">
            <div class="container">
              <div class="carousel-caption" id="alpe" style="top: 0; float: left; text-align: right;">
                <h1>Iz Slovenije lahko letite VSEPOVSOD!</h1>
                <p>V <span style="color:white">NAJHITREJŠEM</span> možnem času, do <span style="color:white">ŽELENE destinacije</span></p>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <img class="third-slide" src="<?php echo base_url('assets/slike/StaroMesto.jpg'); ?>" alt="Third slide">
            <div class="container">
              <div class="carousel-caption text-right">
                <h1>Lahko se odločite za naš POTOVALNI paket!</h1>
                <p>Na destinaciji vam <span style="color:white">ponudimo izbiro</span> več vodenih <span style="color:white">EKSKURZIJ</span>!</p>
                
              </div>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
 
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
</html>