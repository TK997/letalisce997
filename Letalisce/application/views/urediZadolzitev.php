<div class="container" id="klobasa">


    <?php echo form_open('zadolzitve/shraniSpremembeZadolzitve/' . $data->naslov, ['class' => 'horizontal-form']); ?>

    <h3>Uredi Zadolžitev</h3>
    <div style="padding-left: 320px">

        <input type="submit" class="btn btn-outline-success" href="#" value="Uredi" />

    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Naslov</label>
                <div class="col-md-9">

                    <?php echo form_input(['name' => 'naslov', 'class' => 'form-control', 'placeholder' => 'Ime', 'value' => $data->naslov]); ?>
                    <?php echo form_error('ime'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Vsebina</label>
                <div class="col-md-9">

                    <?php echo form_input(['name' => 'vsebina', 'class' => 'form-control', 'placeholder' => 'Priimek', 'value' => $data->vsebina]); ?>
                    <?php echo form_error('priimek'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>

    
    <div class="row">
        <div class="col-md-6">
            <div class="form-group" id="izbira">
                <label class="col-md-3 control-label">Oznaka</label>
                <select class="col-lg-9" name="oznaka">

                    <option value="<?php echo $data->oznaka; ?>"><?php echo $data->oznaka; ?></option>
                    <?php if ($data->oznaka == 'VP') {
                    ?>
                        <option value="SP">SP</option>
                        <option value="MP">MP</option>
                    <?php
                    } elseif($data->oznaka == 'SP') {
                    ?>
                        <option value="VP">VP</option>
                        <option value="MP">MP</option>
                    <?php
                    } else {
                    ?>
                        <option value="VP">VP</option>
                        <option value="SP">SP</option>
                    <?php
                    }
                    ?>

                </select>
                <?php echo form_error('spol'); ?>
            </div>
        </div>
        <div class="col-md-6">

        </div>

    </div>






    <div class="row">
        <div class="col-md-6">
            <div class="form-group" id="izbira">
                <label class="col-md-3 control-label">Status</label>
                <select class="col-lg-9" name="status">

                    <option value="<?php echo $data->status; ?>"><?php echo $data->status; ?></option>
                    <?php if ($data->status == 'Ni odpravljeno') {
                    ?>
                        <option value="odpravljeno">Odpravljeno</option>
                    <?php
                    } elseif ($data->status == 'Odpravljeno') {
                    ?>
                        <option value="Ni odpravljeno">Ni odpravljeno</option>
                    <?php
                    } else {
                    ?>
                        
                    <?php
                    }
                    ?>

                </select>
                <?php echo form_error('status'); ?>
            </div>
        </div>
        <div class="col-md-6">

        </div>

    </div>


    <?php echo form_close(); ?>

</div>