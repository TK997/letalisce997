<div class="container" id="klobasa">


    <?php echo form_open('oglasi/shraniSpremembeOglasa/'.urlencode($data['naslov']), ['class' => 'horizontal-form']); ?>

    <h3>Uredi Oglas</h3>
    <div style="padding-left: 320px">

        <input type="submit" class="btn btn-outline-success" href="#" value="Uredi" />

    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Naslov</label>
                <div class="col-md-9">

                    <?php echo form_input(['name' => 'naslov', 'class' => 'form-control', 'placeholder' => 'Ime', 'value' => $data['naslov']]); ?>
                    <?php echo form_error('ime'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="col-md-3 control-label">Vsebina</label>
                <div class="col-md-12">

                    <?php echo form_input(['name' => 'vsebina', 'class' => 'form-control', 'placeholder' => 'Priimek', 'value' => $data['vsebina']]); ?>
                    <?php echo form_error('vsebina'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>


    <div class="row">
        <div class="col-md-6">
            <div class="form-group" id="izbira">
                <label class="col-lg-3 control-label">Avtor</label>
                    <div class="col-md-9">
                    <?php echo form_input(['name' => 'avtor', 'class' => 'form-control', 'placeholder' => 'avtor', 'value' => $data['avtor']]); ?>
                    <?php echo form_error('avtor'); ?>
                    </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>

    </div>



    <div class="row">
        <div class="col-md-9">
            <div class="form-group" id="izbira">
                <label class="col-md-3 control-label">Datum - |ddmmllll|</label>
                <div class="col-md-6">
                    <?php echo form_input(['name' => 'datum', 'class' => 'form-control', 'placeholder' => 'datum', 'value' => $data['datum']]); ?>
                    <?php echo form_error('datum'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-9">

        </div>

    </div>


    <?php echo form_close(); ?>

</div>