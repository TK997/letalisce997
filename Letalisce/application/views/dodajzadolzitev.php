<div class="container" id="klobasa">


    <?php echo form_open('zadolzitve/shraniNovoZadolzitev/', ['class' => 'horizontal-form']); ?>

    <h3>Dodaj Zadolžitev</h3>
    <div style="padding-left: 320px">

        <input type="submit" class="btn btn-outline-success" href="#" value="Dodaj Nalogo" />

    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Naslov</label>
                <div class="col-md-9">

                    <?php echo form_input(['name' => 'naslov', 'class' => 'form-control', 'placeholder' => 'Ime']); ?>
                    <?php echo form_error('ime'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Vsebina</label>
                <div class="col-md-9">

                    <?php echo form_input(['name' => 'vsebina', 'class' => 'form-control', 'placeholder' => 'Vpiši vsebino']); ?>
                    <?php echo form_error('priimek'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>

    
    <div class="row">
        <div class="col-md-6">
            <div class="form-group" id="izbira">
                <label class="col-md-3 control-label">Oznaka</label>
                <select class="col-lg-9" name="oznaka">
                        
                        <option value="Izberi">Izberi</option>
                        <option value="VP">VP</option>
                        <option value="SP">SP</option>
                        <option value="MP">MP</option>
                    
                </select>
                <?php echo form_error('spol'); ?>
            </div>
        </div>
        <div class="col-md-6">

        </div>

    </div>






    <div class="row">
        <div class="col-md-6">
            <div class="form-group" id="izbira">
                <label class="col-md-3 control-label">Status</label>
                <select class="col-lg-9" name="status">
                
                        <option value="Izberi">Izberi</option>
                        <option value="odpravljeno">Odpravljeno</option>
                        <option value="Ni odpravljeno">Ni odpravljeno</option>
                

                </select>
                <?php echo form_error('status'); ?>
            </div>
        </div>
        <div class="col-md-6">

        </div>

    </div>


    <?php echo form_close(); ?>

</div>