<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Kontrolna Plosca</title>

    <!-- dodani potrebni css/js... -->
    <script src="https://use.fontawesome.com/97b02a79de.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/global.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/carosel.css'); ?>">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstapcn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>



<body class="ozadje">
    <!-- navigacijski panela-->
    <div>
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
            <!-- Dropdown -->
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown">
                      
                    <?php if ($ime) {
                    ?>
                        <a class="navbar-brand nav-item dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button"> Pozdravljeni <?php echo $ime; ?> </a>
                    <?php
                    } else {
                    ?>
                        <a class="navbar-brand nav-item dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button"> Pozdravljeni</a>
                    <?php
                    }
                    ?>
                    
                    </li>
                </ul>
                <!-- konec Dropdown -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <?php if($this->session->userdata('id') == '1') 
                {
                ?>
                <div class="collapse navbar-collapse" id="zaposleni">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <!-- .../letalisce/clanstvo -->
                            <a class="nav-link" href=" <?php echo base_url(); ?>index.php/clanstvo">Zaposleni <span class="sr-only">(current)</span></a>
                        </li>
                
                        <li class="nav-item">
                            <a class="nav-link" href=" <?php echo base_url(); ?>index.php/clanstvo/clani">Člani</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url(); ?>index.php/zadolzitve/pregled">Zadolžitve</a>
                        </li>
                    </ul>
                    
                </div>
                <?php
                }
                else
                {
                ?>
                <div class="collapse navbar-collapse" id="zaposleni">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <!-- .../letalisce/clanstvo -->
                            <a class="nav-link" href="<?php echo base_url(); ?>index.php/oglasi/pregled">Oglasi<span class="sr-only">(current)</span></a>
                        </li>
                
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url(); ?>index.php/prijava">Prijava</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#">Spored Letov</a>
                        </li>
                    </ul>
                    
                </div>

                <?php
                }
                ?>
            </div>
        </nav>
    </div>
    <!-- Konec navigacijskega panela-->
    <div class="wrapper">
        <!-- Sidebar -->
        <?php if($this->session->userdata('level') == '1') 
                {
                ?>
        <nav id="sidebar">

            <ul class="list-unstyled components">
                <li>NAVIGACIJA PO STRANI</li>
                <li>
                    <a href="<?php echo base_url(); ?>index.php/prepoznava">Domov</a>
                </li>
               
            </ul>
        </nav>
                <?php } ?>

    </div>