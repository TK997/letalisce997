<div class="content-wrapper" id="klobasa">


    <div class="dodaja">
        <h3 style="color: red"><?php echo $flashi['neuspesno']; ?></h3>
        <h3 style="color: green"><?php echo $flashi['uspesno']; ?></h3>
        <a class="btn btn-primary" id="DodajNovega" href="<?php echo base_url(); ?>index.php/LetalskeKarte/novaLetKarta"><i class="fa fa-plus"></i> Dodaj Letalsko Karto</a>
    </div>

    <div class="container" id="prikaz">
        <div class="formaa">
            <h3 class="title">Letalske Karte</h3>
        </div>


        <div class="iskalnik">



            <form action="<?php echo base_url() ?>index.php/clanstvo/vrniIskanjeClani" method="POST" id="iskanje">
                <div class="input-group iskalnaLupa">
                    <input type="text" name="iskalniInput" value="" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Iskanje" />
                    <div class="input-group-btn " id="iskalna">
                        <button class="btn btn-sm btn-default"><img src="<?php echo base_url('assets/slike/iskalna-lupa.png'); ?>" width="28" /></button>
                    </div>
                </div>
            </form>

        </div>

        <div class="row">
            <table class="table table-hover">
                <thead>
                    <tr>

                        <th scope="col">ID</th>
                        <th scope="col">Ime</th>
                        <th scope="col">Priimek</th>
                        <th scope="col">Destinacija</th>
                        <th scope="col">Ura_Leta</th>
                        <th scope="col">Datum</th>
                        <th scope="col" style="text-align: center">Opcije</th>

                    </tr>
                </thead>
                <tbody class="tabela">
                    <?php if (count($data)) : ?>
                        <?php foreach ($data as $karta) : ?>
                            <tr>
                                <td><?php echo $karta['id']; ?></td>
                                <td><?php echo $karta['ime']; ?></td>
                                <td><?php echo $karta['priimek']; ?></td>
                                <td><?php echo $karta['destinacija']; ?></td>
                                <td><?php echo $karta['ura_leta']; ?></td>
                                <td><?php echo $karta['datum_leta']; ?></td>
                                <td class="text-center">
                                    <a id="edit" class="btn btn-sm btn-primary" href="<?php echo base_url() . 'index.php/LetalskeKarte/urediLetKarto/'.$karta['id']; ?>" title="Spremeni"><i class="fa fa-pencil"></i></a>
                                    <a class="btn btn-sm btn-danger deleteUser" href="<?php echo base_url() . 'index.php/LetalskeKarte/izbrisiLetKarto/'.$karta['id']; ?>" title="Odstrani"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td>Podatki o članih ne obstajajo!</td>
                            <td><?php echo count($data['clani']); ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
            <div style="padding-left: 50vh">

            </div>
        </div>

    </div>
</div>