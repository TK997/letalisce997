<div class="container" id="klobasa">
    
    <h3 style="color: green"><?php echo $flashi['uspesno']; ?></h3>

    <?php echo form_open('letalskeKarte/shraniurejenoLetKarto/' . $data->id, ['class' => 'horizontal-form']); ?>

    <h3>Uredi Letalsko Karto</h3>
    <div style="padding-left: 320px">

        <input type="submit" class="btn btn-outline-success" href="#" value="Uredi" />

    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Id</label>
                <div class="col-md-9">

                    <?php echo form_input(['name' => 'id', 'class' => 'form-control', 'value' => $data->id]); ?>
                    <?php echo form_error('id'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Ime</label>
                <div class="col-md-9">

                    <?php echo form_input(['name' => 'ime', 'class' => 'form-control', 'placeholder' => 'Ime', 'value' => $data->ime]); ?>
                    <?php echo form_error('ime'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Priimek</label>
                <div class="col-md-9">

                    <?php echo form_input(['name' => 'priimek', 'class' => 'form-control', 'placeholder' => 'Priimek', 'value' => $data->priimek]); ?>
                    <?php echo form_error('priimek'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Destinacija</label>
                <div class="col-md-9">

                    <?php echo form_input(['name' => 'destinacija', 'class' => 'form-control', 'placeholder' => 'Destinacija', 'value' => $data->destinacija]); ?>
                    <?php echo form_error('destinacija'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>


    <div class="row">
        <div class="col-md-6">
            <div class="form-group" id="izbira">
                <label class="col-lg-3 control-label">Ura_Leta</label>
                <div class="col-md-9">
                    <?php echo form_input(['name' => 'ura_leta', 'class' => 'form-control', 'placeholder' => 'Ura', 'value' => $data->ura_leta]); ?>
                    <?php echo form_error('ura_leta'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>

    </div>
    <div class="row">
        <div class="col-md-9">
            <div class="form-group" id="izbira">
                <label class="col-md-3 control-label">Datum - |llllmmdd|</label>
                <div class="col-md-6">
                    <?php echo form_input(['name' => 'datum_leta', 'class' => 'form-control', 'placeholder' => 'Datum', 'value' => $data->datum_leta]); ?>
                    <?php echo form_error('datum_leta'); ?>
                </div>
                <h3 style="color: red"><?php echo $flashi['neuspesno']; ?></h3>
            </div>
        </div>
        <div class="col-md-9">

        </div>

    </div>


    <?php echo form_close(); ?>

</div>