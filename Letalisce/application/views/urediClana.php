<div class="container" id="klobasa">

    <?php echo form_open('clanstvo/shraniSpremembeClana/'.$clani->id, ['class' => 'horizontal-form']); ?>


    <h3>Uredi Clana</h3>
    <div style="padding-left: 320px">

        <input type="submit" class="btn btn-outline-success" href="#" value="Uredi" />

    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Ime</label>
                <div class="col-md-9">

                    <?php echo form_input(['name' => 'ime', 'class' => 'form-control', 'placeholder' => 'Ime', 'value' => $clani->ime]); ?>
                    <?php echo form_error('ime'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Priimek</label>
                <div class="col-md-9">

                    <?php echo form_input(['name' => 'priimek', 'class' => 'form-control', 'placeholder' => 'Priimek', 'value' => $clani->priimek]); ?>
                    <?php echo form_error('priimek'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Email</label>
                <div class="col-lg-9" name="email">

                    <?php echo form_input(['name' => 'email', 'class' => 'form-control', 'placeholder' => 'ime.priimek@xmail.com', 'value' => $clani->email]); ?>
                    <?php echo form_error('email'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Novo Geslo</label>
                <div class="col-lg-9">

                    <?php echo form_input(['name' => 'geslo', 'type' => 'password', 'class' => 'form-control']); ?>
                    <?php echo form_error('geslo'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label class="col-md-5 control-label">Geslo ponovno</label>
                <div class="col-lg-9">

                    <?php echo form_input(['name' => 'geslopon', 'type' => 'password', 'class' => 'form-control', 'placeholder' => 'Vtipkaj geslo ponovno']); ?>
                    <?php echo form_error('geslopon'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group" id="izbira">
                <label class="col-md-3 control-label">Spol</label>
                <select class="col-lg-9" name="spol">

                    <option value="<?php echo $clani->spol; ?>"><?php echo $clani->spol; ?></option>
                    <?php if ($clani->spol == 'Moški') {
                    ?>
                        <option value="Ženska">Ženska</option>
                    <?php
                    } else {
                    ?>
                        <option value="Moški">Moški</option>
                    <?php
                    }
                    ?>

                </select>
                <?php echo form_error('spol'); ?>
            </div>
        </div>
        <div class="col-md-6">

        </div>

    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group" id="izbira">
                <label class="col-md-3 control-label">Rank</label>
                <select class="col-lg-9" name="rank">

                    <option value="<?php echo $clani->level; ?>"><?php echo $clani->level; ?></option>
                    <?php if ($clani->level == 'Admin') {
                    ?>
                        <option value="Zaposleni">Zaposleni</option>
                        <option value="Clan">Član</option>
                    <?php
                    } elseif ($clani->level == 'Zaposleni') {
                    ?>
                        <option value="Admin">Admin</option>
                        <option value="Clan">Član</option>
                    <?php
                    } else {
                    ?>
                        <option value="Admin">Admin</option>
                        <option value="Zaposleni">Zaposleni</option>
                    <?php
                    }
                    ?>

                </select>
                <?php echo form_error('rank'); ?>
            </div>
        </div>
        <div class="col-md-6">

        </div>

    </div>
    <?php echo form_close(); ?>

</div>