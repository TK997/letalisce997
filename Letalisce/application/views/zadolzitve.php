<div class="content-wrapper" id="klobasa">

    
    <div class="dodaja">
    <h3 style="color: red"><?php echo $flashi['neuspesno']; ?></h3>
    <h3 style="color: green"><?php echo $flashi['uspesno']; ?></h3>
        <a class="btn btn-primary" id="DodajNovega" href="<?php echo base_url(); ?>index.php/zadolzitve/dodajzadolzitev"><i class="fa fa-plus"></i> Dodaj Nalogo</a>
    </div>

    <div class="container" id="prikaz">
        <div class="formaa">
            <h3 class="title">Problemi in naloge</h3>
        </div>


        <div class="iskalnik">



            <form action="<?php echo base_url() ?>index.php/clanstvo/vrniIskanjeClani" method="POST" id="iskanje">
                <div class="input-group iskalnaLupa">
                    <input type="text" name="iskalniInput" value="" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Iskanje" />
                    <div class="input-group-btn " id="iskalna">
                        <button class="btn btn-sm btn-default"><img src="<?php echo base_url('assets/slike/iskalna-lupa.png'); ?>" width="28" /></button>
                    </div>
                </div>
            </form>

        </div>

        <div class="row" style="background-color: darkorange"> 
            <table class="table table-hover">
                <thead>
                    <tr>

                        <th scope="col">Naslov</th>
                        <th scope="col">Opis</th>
                        <th scope="col">Pomembnost</th>
                        <th scope="col">Status</th>
                        <th scope="col" style="text-align: center">Opcije</th>
                
                    </tr>
                </thead>
                <tbody class="tabela">
                    <?php if (count($data['zadolzitve'])) : ?>
                        <?php foreach ($data['zadolzitve'] as $zadolzitev) : ?>
                            <?php if ($zadolzitev->oznaka === 'VP') : ?>
                                <tr>

                                    <td><?php echo $zadolzitev->naslov; ?></td>
                                    <td><?php echo $zadolzitev->vsebina; ?></td>
                                    <td><?php echo $zadolzitev->oznaka; ?></td>
                                    <td><?php echo $zadolzitev->status; ?></td>
                                    
                                    <td class="text-center">
                                        <a id="edit" class="btn btn-sm btn-primary" href="<?php echo base_url().'index.php/zadolzitve/urediZadolzitev/'.$zadolzitev->naslov; ?>" title="Spremeni"><i class="fa fa-pencil"></i></a>
                                        <a class="btn btn-sm btn-danger deleteUser" href="<?php echo base_url().'index.php/zadolzitve/izbrisiZadolzitev/'.$zadolzitev->naslov; ?>" title="Odstrani"><i class="fa fa-trash"></i></a>
                                    </td>

                                </tr>
                            <?php else : ?>

                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td>Podatki o članih ne obstajajo!</td>
                            <td><?php echo count($data['zadolzitve']); ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>

            </table>
        </div>

        <br>
        <div class="row" style="background-color: orange">
            <table class="table table-hover">
                <thead>
                    <tr>

                        <th scope="col">Naslov</th>
                        <th scope="col">Opis</th>
                        <th scope="col">Pomembnost</th>
                        <th scope="col">Status</th>
                        <th scope="col" style="text-align: center">Opcije</th>
                
                    </tr>
                </thead>
                <tbody class="tabela">
                    <?php if (count($data['zadolzitve'])) : ?>
                        <?php foreach ($data['zadolzitve'] as $zadolzitev) : ?>
                            <?php if ($zadolzitev->oznaka === 'SP') : ?>
                                <tr>

                                    <td><?php echo $zadolzitev->naslov; ?></td>
                                    <td><?php echo $zadolzitev->vsebina; ?></td>
                                    <td><?php echo $zadolzitev->oznaka; ?></td>
                                    <td><?php echo $zadolzitev->status; ?></td>
                                    
                                    <td class="text-center">
                                    <a id="edit" class="btn btn-sm btn-primary" href="<?php echo base_url().'index.php/zadolzitve/urediZadolzitev/'.$zadolzitev->naslov; ?>" title="Spremeni"><i class="fa fa-pencil"></i></a>
                                        <a class="btn btn-sm btn-danger deleteUser" href="<?php echo base_url().'index.php/zadolzitve/izbrisiZadolzitev/'.$zadolzitev->naslov; ?>" title="Odstrani"><i class="fa fa-trash"></i></a>
                                    </td>

                                </tr>
                            <?php else : ?>

                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td>Podatki o članih ne obstajajo!</td>
                            <td><?php echo count($data['zadolzitve']); ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>

            </table>
        </div>

        <br>
        <div class="row" style="background-color: bisque">
            <table class="table table-hover">
                <thead>
                    <tr>

                        <th scope="col">Naslov</th>
                        <th scope="col">Opis</th>
                        <th scope="col">Pomembnost</th>
                        <th scope="col">Status</th>
                        <th scope="col" style="text-align: center">Opcije</th>
                
                    </tr>
                </thead>
                <tbody class="tabela">
                    <?php if (count($data['zadolzitve'])) : ?>
                        <?php foreach ($data['zadolzitve'] as $zadolzitev) : ?>
                            <?php if ($zadolzitev->oznaka === 'MP') : ?>
                                <tr>

                                    <td><?php echo $zadolzitev->naslov; ?></td>
                                    <td><?php echo $zadolzitev->vsebina; ?></td>
                                    <td><?php echo $zadolzitev->oznaka; ?></td>
                                    <td><?php echo $zadolzitev->status; ?></td>
                                    
                                    <td class="text-center">
                                    <a id="edit" class="btn btn-sm btn-primary" href="<?php echo base_url().'index.php/zadolzitve/urediZadolzitev/'.$zadolzitev->naslov; ?>" title="Spremeni"><i class="fa fa-pencil"></i></a>
                                        <a class="btn btn-sm btn-danger deleteUser" href="<?php echo base_url().'index.php/zadolzitve/izbrisiZadolzitev/'.$zadolzitev->naslov; ?>" title="Odstrani"><i class="fa fa-trash"></i></a>
                                    </td>

                                </tr>
                            <?php else : ?>

                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td>Podatki o članih ne obstajajo!</td>
                            <td><?php echo count($data['zadolzitve']); ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>

            </table>
        </div>

    </div>
</div>