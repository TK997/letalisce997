
<div class='container' style="padding-top: 10vh">
    <div class="dodaja">
        <h3 style="color: red"><?php echo $flashi['neuspesno']; ?></h3>
        <h3 style="color: green"><?php echo $flashi['uspesno']; ?></h3>
        <?php if($this->session->userdata('level') == '2') {
        ?>
        <a class="btn btn-primary" id="DodajNovega" href="<?php echo base_url(); ?>index.php/oglasi/dodajOglas"><i class="fa fa-plus"></i>Napiši oglas</a>
        <a class="btn btn-primary" id="DodajNovega" href="<?php echo base_url(); ?>index.php/oglasi/pregledJSON"><i class="fa fa-plus"></i>JSON</a>
        <hr>
        <?php } ?>
    </div>
    <?php if($this->session->userdata('level') == '2'|| $this->session->userdata('level') == '1') { 
    ?>
    <div class="container" style="padding-top: 10vh">
    <?php } else{ ?>
        <div class="container">
    <?php } ?>
        <div class="row">

            <div class="col-lg-8 col-md-10 mx-auto">
                <?php if (count($data)) : ?>
                    <?php foreach ($data as $oglasinfo) : ?>
                        <div class="post-preview">

                            <h2 style="background-color: #F7652A">
                                <?php echo $oglasinfo['naslov']; ?>

                            </h2>
                            <hr>
                            <h3 class="post-subtitle" style="background-color: #ECA85C">
                                <?php echo $oglasinfo['vsebina']; ?>
                            </h3>

                            <p class="post-meta" style="text-align: center; color: black; background-color: white;">Napisal:
                                <?php echo $oglasinfo['avtor']; ?>
                                <?php echo $oglasinfo['datum']; ?></p>
                            <?php if($this->session->userdata('level') == '2'|| $this->session->userdata('level') == '3' || $this->session->userdata('level') == '1') { 
                            ?>
                            <p style="text-align: center">
                                <a id="edit" class="btn btn-sm btn-primary" href="<?php echo base_url() . 'index.php/oglasi/urediOglas/' . urlencode($oglasinfo['naslov']); ?>" title="Spremeni"><i class="fa fa-pencil"></i></a>
                                <a class="btn btn-sm btn-danger deleteUser" href="<?php echo base_url() . 'index.php/oglasi/izbrisiOglas/' . urlencode($oglasinfo['naslov']); ?>" title="Odstrani"><i class="fa fa-trash"></i></a>

                            </p>
                            <?php } else { ?>
                                <br>
                            <?php } ?>
                            <hr>

                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td>Podatki o oglasih ne obstajajo!</td>
                            <td><?php echo count($data); ?></td>
                        </tr>
                    <?php endif; ?>
                        </div>
                        <hr>
            </div>
        </div>
    </div>
</div>