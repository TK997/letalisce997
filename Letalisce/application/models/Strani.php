<?php
class Strani extends CI_Model
{


    //vrne stevilo vseh zapisov tabele za clane
    //problem je da count_all vrne st seh zapisov v db
    //popravek: deluje kot mora, tako da se vrne pravo stevilo strani
    public function st_zapisovClani()
    {
        $this->db->where('level', 3);
        $this->db->from('uporabniki');
        return $this->db->count_all_results();
    }

    //vrne stevilo zapisov tabele za zaposlene
    //problem je da count_all vrne st seh zapisov v db
    //popravek: deluje kot mora, tako da se vrne pravo stevilo strani
    public function st_zapisovZaposleni()
    {
        $this->db->where('level', 2);
        $this->db->from('uporabniki');
        return $this->db->count_all_results();
    }

    //povprasevanje z lmitiranjem za clane
    public function vrni_poStevilu($limit, $start)
    {

        $this->db->where('level', 3);
        $this->db->limit($limit, $start);
        $this->db->order_by('ime', 'ASC');
        $result = $this->db->get('uporabniki');
        return $result->result();
    }


    //povprasevanje z lmitiranjem za zaposlene
    public function vrni_poSteviluZaposlene($limit, $start)
    {

        $this->db->where('level', 2);
        $this->db->limit($limit, $start);
        $this->db->order_by('ime', 'ASC');
        $result = $this->db->get('uporabniki');
        return $result->result();
    }




    //povprasevanje z limitiranjem za ISKANJE 
    //za clane
    public function vrni_poSteviluIskanjeClani($limit, $start, $iskanje = '')
    {
        $this->db->select(['id', 'ime', 'priimek', 'email', 'spol', 'level']);
        $this->db->from('uporabniki');
        //ce polje ni prazno potem nastavimo kriterije za where
        if (!empty($iskanje)) {
            $this->db->where("ime LIKE '%" . $iskanje . "%' OR email LIKE '%" . $iskanje . "%' OR priimek LIKE '%" . $iskanje . "%'");
        } else {
            $this->db->where('level', 3);
        }
        $this->db->limit($limit, $start);
        $this->db->order_by('ime', 'ASC');
        $result = $this->db->get();
        return $result->result();
    }


    //povprasevanje da dobimo dejansko stevilo uporabnikov -> potrebno da vemo koliko strani zelimo
    //za clane
    public function vrni_steviloPoIskanjuClani($iskanje)
    {
        $this->db->select(['id', 'ime', 'priimek', 'email', 'spol', 'level']);
        //ce polje ni prazno potem nastavimo kriterije za where
        if (!empty($iskanje)) {
            $this->db->where("ime LIKE '%" . $iskanje . "%' OR email LIKE '%" . $iskanje . "%' OR priimek LIKE '%" . $iskanje . "%'");
        } else {
            $this->db->where('level', 3);
        }
        $this->db->from('uporabniki');
        $this->db->order_by('ime', 'ASC');
        $result = $this->db->get();
        return $result->num_rows();
    }



    //povprasevanje z limitiranjem za ISKANJE 
    //za zaposlene
    public function vrni_poSteviluIskanjeZaposleni($limit, $start, $iskanje = '')
    {
        $this->db->select(['id', 'ime', 'priimek', 'email', 'spol', 'level']);
        $this->db->from('uporabniki');
        //ce polje ni prazno potem nastavimo kriterije za where
        if (!empty($iskanje)) {
            $this->db->where("ime LIKE '%" . $iskanje . "%' OR email LIKE '%" . $iskanje . "%' OR priimek LIKE '%" . $iskanje . "%'");
        } else {
            $this->db->where('level', 2);
        }
        $this->db->limit($limit, $start);
        $this->db->order_by('ime', 'ASC');
        $result = $this->db->get();
        return $result->result();
    }


    //povprasevanje da dobimo dejansko stevilo uporabnikov -> potrebno da vemo koliko strani zelimo
    //za zaposlene
    public function vrni_SteviloPoIskanjuZaposleni($iskanje)
    {
        $this->db->select(['id', 'ime', 'priimek', 'email', 'spol', 'level']);
        //ce polje ni prazno potem nastavimo kriterije za where
        if (!empty($iskanje)) {

            $this->db->where("ime LIKE '%" . $iskanje . "%' OR email LIKE '%" . $iskanje . "%' OR priimek LIKE '%" . $iskanje . "%'");
        } else {
            $this->db->where('level', 2);
        }
        $this->db->from('uporabniki');
        $this->db->order_by('ime', 'ASC');
        $result = $this->db->get();
        return $result->num_rows();
    }
}
