<?php 
class ZadolzitveDb extends CI_Model  {

    function vrniZadolzitve() {
        $this->db->select(['naslov', 'vsebina', 'oznaka', 'status']);
        $this->db->from('zadolzitve');
        $res = $this->db->get();
        return $res->result();
    }


    function vrniSpecificnoZadolzitev($naslov) {
        $this->db->select(['naslov', 'vsebina', 'oznaka', 'status']);
        $this->db->from('zadolzitve');
        $this->db->where('naslov', $naslov);
        $res = $this->db->get();
        return $res->result();
    }

    function spremeni_zadolzitev($podatki, $naslov) {
        
        return $this->db->where('naslov', $naslov)
                    ->update('zadolzitve', $podatki);

    }

    function odstraniZadolzitev($naslov) {
        $this->db->where('naslov', $naslov);
        return $this->db->delete('zadolzitve');
    }

    function dodaj_zadolzitev($podatki) {
        
        return $this->db->insert('zadolzitve', $podatki);

    }
}