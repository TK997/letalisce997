<?php 
class OglasiDb extends CI_Model {

    function vrniSpecificneOglase($imeSeje) {
        $this->db->select(['naslov', 'vsebina', 'avtor', 'datum']);
        $this->db->from('oglasi');
        $this->db->join('uporabniki', 'uporabniki.ime = oglasi.avtor');
        $this->db->where('uporabniki.ime', $imeSeje);
        $s = $this->db->get();
        return $s->result_array();
        

    }


    function vrniVseOglase() {
        $this->db->select(['naslov', 'vsebina', 'avtor', 'datum']);
        $this->db->from('oglasi');
        $s = $this->db->get();
        return $s->result_array();
        

    }

    function urejanjeOglasa($naslov) {
        $this->db->select(['naslov', 'vsebina', 'avtor', 'datum']);
        $this->db->from('oglasi');
        $this->db->where('naslov', $naslov);
        $rez = $this->db->get();
        return $rez->result_array();

    }

    function shraniSpremembeOglasa($naslov, $podatki) {
        return $this->db->where('naslov', $naslov)
                        ->update('oglasi', $podatki);
    }

    function izbrisiOglas($naslov) {
        $this->db->where('naslov', $naslov);
        return $this->db->delete('oglasi');
    }


    function dodajOglas($podatki) {
        return $this->db->insert('oglasi', $podatki);
    }
}