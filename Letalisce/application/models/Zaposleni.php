<?php
class Zaposleni extends CI_Model {

    function vrni_zaposlene() {
        $this->db->select(['id', 'ime', 'priimek', 'email', 'level', 'spol']);
        $this->db->from('uporabniki');
        $res = $this->db->get();
        return $res->result();
    }




    //dodaj zaposlene v tabelo preko forme
    function dodaj_zaposlene($zaposleni) {
        
        return $this->db->insert('uporabniki', $zaposleni);

    }

    function odstraniUporabnika($id) {
        $this->db->where('id', $id);
        return $this->db->delete('uporabniki');
    }

    function spremeni_zaposlene($podatki, $id) {
        
        return $this->db->where('id', $id)
                    ->update('uporabniki', $podatki);

    }


    function vrni_člane() {
        $this->db->select(['id', 'ime', 'priimek', 'email', 'level', 'spol']);
        $this->db->from('uporabniki');
        $res = $this->db->get();
        return $res->result();
    }

    function dodaj_clana($clan) {
        
        return $this->db->insert('uporabniki', $clan);

    }


    function vrniSpecificnegaUporabnika($id) {
        $this->db->select(['id', 'ime', 'priimek', 'email', 'geslo', 'level', 'spol']);
        $this->db->from('uporabniki');
        $this->db->where('id', $id);
        $res = $this->db->get();
        return $res->result();
    }
    
    
}