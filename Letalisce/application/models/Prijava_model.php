<?php
class Prijava_model extends CI_Model {

    function poskus_prijave($email, $geslo) {
        $this->db->where('email', $email);
        $this->db->where('geslo', $geslo);
        $res = $this->db->get('uporabniki', 1);
        return $res;
    }

    function spremeni_geslo($id, $podatki) {
        return $this->db->where('id', $id)
                        ->update('uporabniki', $podatki);
    }

    function vrniGeslo($id) {
        $this->db->select('geslo');
        $this->db->from('uporabniki');
        $this->db->where('id', $id);
        $s = $this->db->get();
        return $s->result_array();
    }
}