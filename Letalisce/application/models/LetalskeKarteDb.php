<?php
class LetalskeKarteDb extends CI_Model {

    function vrniLetKarte() {
        
        $this->db->from('letalske_karte');
        $s = $this->db->get();
        return $s->result_array();   
    }


    function vrniLetKartaId() {
        $this->db->limit(1);
        $this->db->order_by('id', 'DESC');
        $s = $this->db->get('letalske_karte');
        return $s->result();
    }


    function dodajNovoLetKarto($podatki) {
        return $this->db->insert('letalske_karte', $podatki);
    }

    function vrniLetKartoSpec($id) {
        $this->db->where('id', $id);
        $s = $this->db->get('letalske_karte');
        return $s->result();
    }

    function urediLetKarto($id, $podatki) {
        return $this->db->where('id', $id)
                        ->update('letalske_karte', $podatki);
    }

    function izbrisiLetKarto($id) {
        $this->db->where('id', $id);
        return $this->db->delete('letalske_karte');
    }
}