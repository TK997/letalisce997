<?php
class Oglasi extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('oglasiDb');
        $this->load->helper('file');

        if ($this->session->userdata('prijavljen') !== TRUE) {
            redirect('PrvaStran/');
          }
    }


    public function vrni_ime()
    {

        $imenik = array(
            'ime' => $this->session->userdata('ime')
        );

        return $imenik;
    }
    

    function pregledJSON()
    {

        $ime = $this->vrni_ime();

        $userdata['ime'] = $this->session->userdata('ime');
        if($this->session->userdata('level') == '2') {
            $data = $this->oglasiDb->vrniSpecificneOglase($userdata['ime']);
        } else {
            $data = $this->oglasiDb->vrniVseOglase();
        }
        
        $flashi = array(
            'uspesno' => $this->session->flashdata('uspesno'),
            'neuspesno' => $this->session->flashdata('neuspesno')

        );
        
        $dataJSON = json_encode($data,TRUE);
        //exit();
        //$jsondatoteka = './oglasi_json.json';
        //echo ($jsondatoteka);
        //print_r( json_encode($data));
        //exit();

        if(write_file('oglasi.json', $dataJSON)) {
            echo "RATALO";
        } else {
            echo "NI RATALO";
        }
    
        if ($this->session->userdata('level') == '1' || $this->session->userdata('level') == '2') {
            $this->load->view('templates/glava', $ime);
            $this->load->view('oglasi', ['data' => $data, 'flashi' => $flashi]);
            $this->load->view('templates/noga');
        } else {
            $this->load->view('templates/glava_prijavna', $ime);
            $this->load->view('oglasi', ['data' => $data, 'flashi' => $flashi]);
            $this->load->view('templates/noga');
        }
    }



    function pregled()
    {

        $ime = $this->vrni_ime();

        $userdata['ime'] = $this->session->userdata('ime');
        if($this->session->userdata('level') == '2') {
            $data = $this->oglasiDb->vrniSpecificneOglase($userdata['ime']);
        } else {
            $data = $this->oglasiDb->vrniVseOglase();
        }
        
        $flashi = array(
            'uspesno' => $this->session->flashdata('uspesno'),
            'neuspesno' => $this->session->flashdata('neuspesno')

        );




        //echo '<pre>';
        //print_r($data);
        //echo '</pre>';
        //exit();

        if ($this->session->userdata('level') == '1' || $this->session->userdata('level') == '2') {
            $this->load->view('templates/glava', $ime);
            $this->load->view('oglasi', ['data' => $data, 'flashi' => $flashi]);
            $this->load->view('templates/noga');
        } else {
            $this->load->view('templates/glava_prijavna', $ime);
            $this->load->view('oglasi', ['data' => $data, 'flashi' => $flashi]);
            $this->load->view('templates/noga');
        }
    }




    function urediOglas($naslov)
    {


        //tu je naslov se zakodiran v url : + in !.
        $naslova = urldecode($naslov);
        //tu je naslov odkodiran iz urlja v cisti text, kar rabim za vnrjenje iz baze podatkov
        $ime = $this->vrni_ime();



        $data = $this->oglasiDb->urejanjeOglasa($naslova);


        //echo '<pre>';
        //print_r($data[0]);
        //echo '</pre>';
        //exit();


        $this->load->view('templates/glava', $ime);
        $this->load->view('urediOglas', ['data' => $data[0]]);
        $this->load->view('templates/noga');
    }


    function shraniSpremembeOglasa($naslov)
    {


        $ime = $this->vrni_ime();

        $podatki = array(
            'naslov' => $this->input->post('naslov'),
            'vsebina' => $this->input->post('vsebina'),
            'avtor' => $this->input->post('avtor'),
            'datum' => $this->input->post('datum')
        );


        //echo '<pre>';
        //print_r(urldecode($naslov));
        //echo '</pre>';
        //exit();

        //dekodiramo url naslov, da se znebimo + in !
        if ($this->oglasiDb->shraniSpremembeOglasa(urldecode($naslov), $podatki)) {

            //echo '<pre>';
            //print_r($podatki);
            //echo '</pre>';
            //exit();
            $this->session->set_flashdata('uspesno', 'Oglas USPESNO spremenjen!');
        } else {
            //echo '<pre>';
            //print_r('neuspesno');
            //echo '</pre>';
            //exit();
            $this->session->set_flashdata('neuspesno', 'Oglas NI bil uspesno spremenjen!');
        }





        //po spremembi brez veze, da se vrnemo na se enkrat spreminjanje
        //return redirect(base_url().'index.php/clanstvo/urediZaposlenega/'.$id);
        return redirect(base_url() . 'index.php/oglasi/pregled');
    }

    function izbrisiOglas($naslov)
    {

        //echo '<pre>';
        //print_r($naslov);
        //echo '</pre>';
        //exit();
        $this->oglasiDb->izbrisiOglas(urldecode($naslov));
        redirect('oglasi/pregled');
    }


    function dodajOglas()
    {
        $ime = $this->vrni_ime();
        $avtor = $this->session->userdata('ime');

        $this->load->view('templates/glava', $ime);
        $this->load->view('dodajOglas', ['avtor' => $avtor]);
        $this->load->view('templates/noga');
    }


    function dodajanjeOglasa()
    {



        $podatki = array(
            'naslov' => $this->input->post('naslov'),
            'vsebina' => $this->input->post('vsebina'),
            'avtor' => $this->input->post('avtor'),
            'datum' => $this->input->post('datum')
        );





        //echo '<pre>';
        //print_r(urldecode($naslov));
        //echo '</pre>';
        //exit();

        //dekodiramo url naslov, da se znebimo + in !
        if ($this->oglasiDb->dodajOglas($podatki)) {

            //echo '<pre>';
            //print_r($podatki);
            //echo '</pre>';
            //exit();
            $this->session->set_flashdata('uspesno', 'Oglas USPESNO napisan!');
        } else {
            //echo '<pre>';
            //print_r('neuspesno');
            //echo '</pre>';
            //exit();
            $this->session->set_flashdata('neuspesno', 'Oglas NI bil uspesno napisan!');
        }





        //po spremembi brez veze, da se vrnemo na se enkrat spreminjanje
        //return redirect(base_url().'index.php/clanstvo/urediZaposlenega/'.$id);
        return redirect(base_url() . 'index.php/oglasi/pregled');
    }
}
