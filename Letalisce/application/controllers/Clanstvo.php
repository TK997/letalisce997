<?php
class Clanstvo extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    //nalozim zaposleni model
    $this->load->model('zaposleni');
    $this->load->model('strani');

    if ($this->session->userdata('prijavljen') !== TRUE) {
      redirect('PrvaStran');
    }
  }

  function index()
  {
    //kr takoj vrzemo na prijavno stran/ mogoce bom spremenil
    $this->load->view('zaposleni');
  }



  public function vrni_ime()
  {

    $imenik = array(
      'ime' => $this->session->userdata('ime')
    );

    return $imenik;
  }



  function pripravi_člane()
  {
    $člani = $this->zaposleni->vrni_člane();
    return $člani;
    /*
  echo '<pre>';
  print_r($zaposleni);
  echo '</pre>';
  echo count($zaposleni);
  $this->load->view('rank_dostopa/admin_view');
  */
  }


  function vrniIskanjeClani()
  {
    $ime = $this->vrni_ime();
    $kar_iscemo = $this->input->post('iskalniInput');

    //podatke, ki jih bomo poslali na view so shranjeni v podatkih[]
    $podatki['kar_iscemo'] = $kar_iscemo;

    $stevilo_vseh_rezultatov = $this->strani->vrni_steviloPoIskanjuClani($kar_iscemo);
    $na_stran = 5;

    $nastavitve = array(
      'base_url' => base_url() . "index.php/clanstvo/clani",
      'total_rows' => $stevilo_vseh_rezultatov,
      'per_page' => $na_stran,
      'uri_segment' => 3,
      //jezik
      'first_link' => "Prva",
      'last_link' => "Zadnja"
    );

    //inicializacija prikazovanja vec strani zaposlenih:
    $this->pagination->initialize($nastavitve);

    $segment = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

    $data["links"] = $this->pagination->create_links();

    $data['clani'] = $this->strani->vrni_poSteviluIskanjeClani($nastavitve['per_page'], $segment, $kar_iscemo);

    //echo '<pre>';
    //print_r($data['clani']);
    //echo '</pre>';
    //exit();
    
    if ($data['clani'] > 0) {
      $this->session->set_flashdata('uspesno', 'Iskanje USPESNO!');
    } else {
      $this->session->set_flashdata('neuspesno', 'Iskanje NI bilo uspesno!');
    }

    $flashi = array(
      'uspesno' => $this->session->flashdata('uspesno'),
      'neuspesno' => $this->session->flashdata('neuspesno')

    );

    $this->load->view('templates/glava', $ime);
    // zraven, da bom bral podatke posljem array zaposlenih:
    $this->load->view('clani', ['flashi' => $flashi, 'data' => $data]);

    $this->load->view('templates/noga');
  }



  function vrniIskanjeZaposleni()
  {
    $ime = $this->vrni_ime();
    $kar_iscemo = $this->input->post('iskalniInput');

    //podatke, ki jih bomo poslali na view so shranjeni v podatkih[]
    $podatki['kar_iscemo'] = $kar_iscemo;


    $stevilo_vseh_rezultatov = $this->strani->vrni_SteviloPoIskanjuZaposleni($kar_iscemo);
    $na_stran = 5;

    $nastavitve = array(
      'base_url' => base_url() . "index.php/clanstvo/zaposleni",
      'total_rows' => $stevilo_vseh_rezultatov,
      'per_page' => $na_stran,
      'uri_segment' => 3,
      //jezik
      'first_link' => "Prva",
      'last_link' => "Zadnja"
    );


   
    

    //inicializacija prikazovanja vec strani zaposlenih:
    $this->pagination->initialize($nastavitve);

    $segment = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

    $data["links"] = $this->pagination->create_links();

    $data['zaposleni'] = $this->strani->vrni_poSteviluIskanjeZaposleni($nastavitve['per_page'], $segment, $kar_iscemo);

    //echo '<pre>';
    //print_r($data['zaposleni']);
    //echo '</pre>';
    //exit();
    

    
    if ($data['zaposleni'] > 0) {
      $this->session->set_flashdata('uspesno', 'Iskanje USPESNO!');
    } else {
      $this->session->set_flashdata('neuspesno', 'Iskanje NI bilo uspesno!');
    }

    $flashi = array(
      'uspesno' => $this->session->flashdata('uspesno'),
      'neuspesno' => $this->session->flashdata('neuspesno')

    );

    $this->load->view('templates/glava', $ime);
    // zraven, da bom bral podatke posljem array zaposlenih:
    $this->load->view('zaposleni', ['flashi' => $flashi, 'data' => $data]);

    $this->load->view('templates/noga');
  }











  public function clani()
  {

    $ime = $this->vrni_ime();
    //$clani = $this->pripravi_člane();
    $flashi = array(
      'uspesno' => $this->session->flashdata('uspesno'),
      'neuspesno' => $this->session->flashdata('neuspesno')

    );

    //konfiguracija kreiranja linkov za strani
    $stevilo_vseh_rezultatov = $this->strani->st_zapisovClani();
    $na_stran = 5;

    $nastavitve = array(
      'base_url' => base_url() . "index.php/clanstvo/clani",
      'total_rows' => $stevilo_vseh_rezultatov,
      'per_page' => $na_stran,
      'uri_segment' => 3,
      //jezik
      'first_link' => "Prva",
      'last_link' => "Zadnja"
    );

    

    //inicializacija prikazovanja vec strani zaposlenih:
    $this->pagination->initialize($nastavitve);
    $segment = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

    $data['links'] = $this->pagination->create_links();

    $data['clani'] = $this->strani->vrni_poStevilu($nastavitve['per_page'], $segment);

    //echo '<pre>';
    //print_r (floor($totalRow));
    //echo '</pre>';
    //exit();

    $this->load->view('templates/glava', $ime);
    // zraven, da bom bral podatke posljem array zaposlenih:
    $this->load->view('clani', ['data' => $data, 'flashi' => $flashi]);
    $this->load->view('templates/noga');
  }


  public function dodajClan()
  {

    $ime = $this->vrni_ime();



    $this->load->view('templates/glava', $ime);
    $this->load->view('dodajClana');
    $this->load->view('templates/noga');
  }




  function urediClana($id)
  {
    $ime = $this->vrni_ime();
    $clan = $this->zaposleni->vrniSpecificnegaUporabnika($id);

    /*
    echo '<pre>';
    print_r($zaposlen);
    echo '</pre>';
    exit();
    */

    $specificnClan = $clan[0];

    $flashi = array(
      'uspesno' => $this->session->flashdata('uspesno'),
      'neuspesno' => $this->session->flashdata('neuspesno')

    );

    /*
    echo '<pre>';
    print_r($specificnZaposlen);
    echo '</pre>';
    exit();
    */


    if ($specificnClan->spol === "M") {
      $specificnClan->spol = "Moški";
    } else {
      $specificnClan->spol = "Ženska";
    }

    if ($specificnClan->level === '2') {
      $specificnClan->level = "Zaposleni";
    } elseif ($specificnClan->level === '3') {
      $specificnClan->level = "Član";
    } else {
      $specificnClan->level = "Admin";
    }


    $this->load->view('templates/glava', $ime);
    // zraven, da bom bral podatke posljem array zaposlenih:
    $this->load->view('urediClana', ['clani' => $specificnClan, 'flashi' => $flashi]);
    $this->load->view('templates/noga');
  }




  public function dodajClana()
  {

    $ime = $this->vrni_ime();

    $this->form_validation->set_rules('ime', 'Ime', 'required');
    $this->form_validation->set_rules('priimek', 'Priimek', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
    $this->form_validation->set_rules('geslo', 'Geslo', 'required');
    $this->form_validation->set_rules('geslopon', 'Ponovno geslo', 'required');
    $this->form_validation->set_rules('spol', 'Spol', 'required');
    $this->form_validation->set_rules('rank', 'Položaj', 'required');

    $this->form_validation->set_message('required', 'Polje {field} je potrebno izpolniti!');
    $this->form_validation->set_message('valid_email', 'Prosim vpišite PRAVILEN mail format!');


    $this->form_validation->set_error_delimiters('<div id="napaka">', '</div>');
    if ($this->form_validation->run() !== FALSE) {

      //vsi podatki, razen ponovnega gesla
      $podatki = array(
        'ime' => $this->input->post('ime'),
        'priimek' => $this->input->post('priimek'),
        'email' => $this->input->post('email'),
        'geslo' => md5($this->input->post('geslo')),
        'spol' => $this->input->post('spol'),
        'level' => $this->input->post('rank')

      );

      if ($podatki['level'] == 'Član') {
        $podatki['level'] = 3;
      }

      if ($podatki['spol'] == 'Ženska') {
        $podatki['spol'] = 'Ž';
      } else {
        $podatki['spol'] = 'M';
      }
      /*
      echo '<pre>';
      print_r($podatki);
      echo '</pre>';
      exit();
      */
      if ($this->zaposleni->dodaj_clana($podatki)) {
        $this->session->set_flashdata('uspesno', 'Uporabnik USPESNO dodan!');
      } else {
        $this->session->set_flashdata('neuspesno', 'Uporabnik NI bil uspesno dodan!');
      }
      return redirect('clanstvo/clani');
    } else {

      $this->dodajClan();
    }
  }



  function shraniSpremembeClana($id)
  {


    $ime = $this->vrni_ime();
    $this->load->library('form_validation');
    $this->form_validation->set_rules('ime', 'Ime', 'required');
    $this->form_validation->set_rules('priimek', 'Priimek', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
    //$this->form_validation->set_rules('geslo', 'Geslo', 'required');
    //$this->form_validation->set_rules('geslopon', 'Ponovno geslo', 'required');
    $this->form_validation->set_rules('spol', 'Spol', 'required');
    $this->form_validation->set_rules('rank', 'Položaj', 'required');

    $this->form_validation->set_message('required', 'Polje {field} je potrebno izpolniti!');
    $this->form_validation->set_message('valid_email', 'Prosim vpišite PRAVILEN mail format!');


    $this->form_validation->set_error_delimiters('<div id="napaka">', '</div>');
    if ($this->form_validation->run() !== FALSE) {

      //vsi podatki, razen ponovnega gesla
      $podatki = array(
        'ime' => $this->input->post('ime'),
        'priimek' => $this->input->post('priimek'),
        'email' => $this->input->post('email'),
        'geslo' => md5($this->input->post('geslo')),
        'spol' => $this->input->post('spol'),
        'level' => $this->input->post('rank')

      );


      if ($podatki['level'] == 'Zaposleni') {
        $podatki['level'] = 2;
      } elseif ($podatki['level'] == 'Član') {
        $podatki['level'] = 3;
      } else {
        $podatki['level'] = 1;
      }

      if ($podatki['spol'] == 'Ženska') {
        $podatki['spol'] = 'Ž';
      } else {
        $podatki['spol'] = 'M';
      }


      if ($this->zaposleni->spremeni_zaposlene($podatki, $id)) {
        $this->session->set_flashdata('uspesno', 'Uporabnik USPESNO spremenjen!');
      } else {
        $this->session->set_flashdata('neuspesno', 'Uporabnik NI bil uspesno spremenjen!');
      }


      //po spremembi brez veze, da se vrnemo na se enkrat spreminjanje
      //return redirect(base_url().'index.php/clanstvo/urediZaposlenega/'.$id);
      return redirect(base_url() . 'index.php/clanstvo/clani');
    } else {


      $this->urediClana($id);
    }
  }



  function pripravi_zaposlene()
  {
    $zaposleni = $this->zaposleni->vrni_zaposlene();
    return $zaposleni;
    /*
  echo '<pre>';
  print_r($zaposleni);
  echo '</pre>';
  echo count($zaposleni);
  $this->load->view('rank_dostopa/admin_view');
  */
  }


  public function zaposleni()
  {

    $ime = $this->vrni_ime();
    //$data['zaposleni'] = $this->pripravi_zaposlene();
    $flashi = array(
      'uspesno' => $this->session->flashdata('uspesno'),
      'neuspesno' => $this->session->flashdata('neuspesno')

    );


    //konfiguracija kreiranja linkov za strani
    $stevilo_vseh_rezultatov = $this->strani->st_zapisovZaposleni();
    $na_stran = 5;

    //za ustvarit strani, predvsem zaradi manjsega vrsticnega okna
    $nastavitve = array(
      'base_url' => base_url() . "index.php/clanstvo/zaposleni",
      'total_rows' => $stevilo_vseh_rezultatov,
      'per_page' => $na_stran,
      'uri_segment' => 3,
      //jezik
      'first_link' => "Prva",
      'last_link' => "Zadnja"
    );

    //inicializacija prikazovanja vec strani zaposlenih:
    $this->pagination->initialize($nastavitve);
    $segment = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

    $data["links"] = $this->pagination->create_links();

    $data['zaposleni'] = $this->strani->vrni_poSteviluZaposlene($nastavitve['per_page'], $segment);

    //echo '<pre>';
    //print_r($stevilo_vseh_rezultatov);
    //echo '</pre>';
    //exit();

    $this->load->view('templates/glava', $ime);
    // zraven, da bom bral podatke posljem array zaposlenih:
    $this->load->view('zaposleni', ['flashi' => $flashi, 'data' => $data]);

    $this->load->view('templates/noga');
  }


  public function dodajZaposlenega()
  {

    $ime = $this->vrni_ime();



    $this->load->view('templates/glava', $ime);
    $this->load->view('dodajZaposlenega');
    $this->load->view('templates/noga');
  }

  public function dodajaZap()
  {

    $ime = $this->vrni_ime();
    $this->load->library('form_validation');
    $this->form_validation->set_rules('ime', 'Ime', 'required');
    $this->form_validation->set_rules('priimek', 'Priimek', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
    $this->form_validation->set_rules('geslo', 'Geslo', 'required');
    $this->form_validation->set_rules('geslopon', 'Ponovno geslo', 'required');
    $this->form_validation->set_rules('spol', 'Spol', 'required');
    $this->form_validation->set_rules('rank', 'Položaj', 'required');

    $this->form_validation->set_message('required', 'Polje {field} je potrebno izpolniti!');
    $this->form_validation->set_message('valid_email', 'Prosim vpišite PRAVILEN mail format!');


    $this->form_validation->set_error_delimiters('<div id="napaka">', '</div>');
    if ($this->form_validation->run() !== FALSE) {

      //vsi podatki, razen ponovnega gesla
      $podatki = array(
        'ime' => $this->input->post('ime'),
        'priimek' => $this->input->post('priimek'),
        'email' => $this->input->post('email'),
        'geslo' => md5($this->input->post('geslo')),
        'spol' => $this->input->post('spol'),
        'level' => $this->input->post('rank')

      );

      if ($podatki['level'] == 'Zaposleni') {
        $podatki['level'] = 2;
      }

      if ($podatki['spol'] == 'Ženska') {
        $podatki['spol'] = 'Ž';
      } else {
        $podatki['spol'] = 'M';
      }
      /*
      echo '<pre>';
      print_r($podatki);
      echo '</pre>';
      exit();
      */
      if ($this->zaposleni->dodaj_zaposlene($podatki)) {
        $this->session->set_flashdata('uspesno', 'Uporabnik USPESNO dodan!');
      } else {
        $this->session->set_flashdata('neuspesno', 'Uporabnik NI bil uspesno dodan!');
      }
      return redirect('clanstvo');
    } else {

      $this->dodajZaposlenega();
    }
  }






  function urediZaposlenega($id)
  {
    $ime = $this->vrni_ime();
    $zaposlen = $this->zaposleni->vrniSpecificnegaUporabnika($id);

    /*
    echo '<pre>';
    print_r($zaposlen);
    echo '</pre>';
    exit();
    */

    $specificnZaposlen = $zaposlen[0];

    $flashi = array(
      'uspesno' => $this->session->flashdata('uspesno'),
      'neuspesno' => $this->session->flashdata('neuspesno')

    );

    /*
    echo '<pre>';
    print_r($specificnZaposlen);
    echo '</pre>';
    exit();
    */


    if ($specificnZaposlen->spol === "M") {
      $specificnZaposlen->spol = "Moški";
    } else {
      $specificnZaposlen->spol = "Ženska";
    }

    if ($specificnZaposlen->level === '2') {
      $specificnZaposlen->level = "Zaposleni";
    } elseif ($specificnZaposlen->level === '3') {
      $specificnZaposlen->level = "Član";
    } else {
      $specificnZaposlen->level = "Admin";
    }





    $this->load->view('templates/glava', $ime);
    // zraven, da bom bral podatke posljem array zaposlenih:
    $this->load->view('uredizaposlenega', ['zaposleni' => $specificnZaposlen, 'flashi' => $flashi]);
    $this->load->view('templates/noga');
  }




  function shraniSpremembeZaposlenega($id)
  {



    $ime = $this->vrni_ime();
    $this->load->library('form_validation');
    $this->form_validation->set_rules('ime', 'Ime', 'required');
    $this->form_validation->set_rules('priimek', 'Priimek', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
    //$this->form_validation->set_rules('geslo', 'Geslo', 'required');
    //$this->form_validation->set_rules('geslopon', 'Ponovno geslo', 'required');
    $this->form_validation->set_rules('spol', 'Spol', 'required');
    $this->form_validation->set_rules('rank', 'Položaj', 'required');

    $this->form_validation->set_message('required', 'Polje {field} je potrebno izpolniti!');
    $this->form_validation->set_message('valid_email', 'Prosim vpišite PRAVILEN mail format!');


    $this->form_validation->set_error_delimiters('<div id="napaka">', '</div>');
    if ($this->form_validation->run() !== FALSE) {

      //vsi podatki, razen ponovnega gesla
      $podatki = array(
        'ime' => $this->input->post('ime'),
        'priimek' => $this->input->post('priimek'),
        'email' => $this->input->post('email'),
        'geslo' => md5($this->input->post('geslo')),
        'spol' => $this->input->post('spol'),
        'level' => $this->input->post('rank')

      );

      if ($podatki['level'] == 'Zaposleni') {
        $podatki['level'] = 2;
      } elseif ($podatki['level'] == "Clan") {
        $podatki['level'] = 3;
      } else {
        $podatki['level'] = 1;
      }

      if ($podatki['spol'] == 'Ženska') {
        $podatki['spol'] = 'Ž';
      } else {
        $podatki['spol'] = 'M';
      }





      if ($this->zaposleni->spremeni_zaposlene($podatki, $id)) {
        $this->session->set_flashdata('uspesno', 'Uporabnik USPESNO spremenjen!');
      } else {
        $this->session->set_flashdata('neuspesno', 'Uporabnik NI bil uspesno spremenjen!');
      }


      //po spremembi brez veze, da se vrnemo na se enkrat spreminjanje
      //return redirect(base_url().'index.php/clanstvo/urediZaposlenega/'.$id);
      return redirect(base_url() . 'index.php/clanstvo');
    } else {


      $this->urediZaposlenega($id);
    }
  }

  function izbrisiUporabnika($id, $rank)
  {
    $this->zaposleni->odstraniUporabnika($id);
    if ($rank === '3') {
      return redirect('clanstvo/clani');
    } elseif ($rank === '2') {
      return redirect('clanstvo');
    } else {
      return redirect('clanstvo/admin');
    }
  }
}
