<?php
class Prijava extends CI_Controller{
  function __construct(){
    parent::__construct();
    //nalozim login model
    $this->load->model('prijava_model');

  }
 
  function index(){
      //kr takoj vrzemo na prijavno stran/ mogoce bom spremenil
    $this->load->view('prijavna_stran');
  }
 
  function avtentikacija_podatkov(){
    $email = $this->input->post('email',TRUE);
    //geslo je vzeto iz form inputa 'geslo' z metodo post in ga md5 enkriptamo zaradi avtentikacije
    $geslo = md5($this->input->post('geslo',TRUE));
    $poskus_prijave = $this->prijava_model->poskus_prijave($email,$geslo);

    //ce je prijava uspesna
    if($poskus_prijave->num_rows() > 0){
        //vrnemo v spremenljivki data prvo vrstico, kjer so ustrezni podatki
        $data  = $poskus_prijave->row_array();


         /*
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        exit();
        */
    
        $id = $data['id'];
        $ime  = $data['ime'];
        $email = $data['email'];
        $level = $data['level'];
        $podatki_seje = array(

            'id' => $id,
          //seja: ime
            'ime'  => $ime,
          //seja: email
            'email'     => $email,
          //seja: level dostopa
            'level'     => $level,
          //seja: ali je uporabnik trenutno prijavljen ali ne
            'prijavljen' => TRUE
        );
        $this->session->set_userdata($podatki_seje);
        // ce je admin, odpri adminski dostop preko kontrolerja Prepoznava
        if($level === '1'){
            redirect('prepoznava');
 
        // ce je usluzbenec IT podrocja/ moderator, odpri moderatorski dostop preko kontrolerja Prepoznava
        }elseif($level === '2'){
            redirect('prepoznava');
 
        // ce je clan odpri clanski dostop preko kontrolerja Prepoznava
        }else{
            redirect('prepoznava');
        }
    }else{
      //ce se neuspesno provamo prijavit
        echo $this->session->set_flashdata('napaka',"<div style='color:white;'>Podani podatki niso pravilni</div>");
        
        redirect('prijava');
    }
  }
 

  //funkcija za odjavljanje
  //-> zbrisemo session -> posljemo na login ali pa home
  function odjava(){
      $this->session->sess_destroy();
      redirect('prijava');
  }


  function spremeniGeslo($id) {

    $geslo = $this->session->userdata('ime');

    //spremenjeno geslo naj bo kar ime na istem iDju

        //echo '<pre>';
        //print_r($geslo);
        //echo '</pre>';
        //exit();
        
    $this->prijava_model->spremeni_geslo($id, $geslo);
    $this->session->session_destroy();
    redirect('prijava');
  }
 
}