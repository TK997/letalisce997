<?php
class PrvaStran extends CI_Controller {

    function __construct()
  {
    parent::__construct();
  }

    function pozdrav() {

        $uporIme = $this->session->userdata('ime');
        $this->load->view('/templates/glava_prijavna', ['ime' => $uporIme]);
        $this->load->view('prva_stran');
    }
}