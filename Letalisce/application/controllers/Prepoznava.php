<?php
class Prepoznava extends CI_Controller{

    function __construct(){
        parent::__construct();
        //ce zastavica prijavljen ni nastavljena na TRUE, potem ocitno nismo prijavljeni in se prijava poskusi ponovno
        if($this->session->userdata('prijavljen') !== TRUE){
          redirect('prijava');
        }

        
    }



    public function vrni_ime() {

        $imenik = array(
            'ime' => $this->session->userdata('ime')
        );
        
        return $imenik; 
    }
     
    

    function index(){
        
        
        $ime = $this->vrni_ime();
        
        //Dovolimo za Admine in za moderatorje/ zaposlene (da urejajo novice, oglase, cene kart itd...)
        if($this->session->userdata('level')==='1' || $this->session->userdata('level') === '2'){

            //za poklicati funkcije moramo prvo nasloviti $this (ta objekt)
            
            
            $this->load->view('templates/glava', $ime);
            $this->load->view('rank_dostopa/admin_view');
            $this->load->view('templates/noga');

        }elseif($this->session->userdata('level') === '3') {
            //$this->load->view('templates/glava', $ime);
            echo "Uporabnik";
            //$this->load->view('templates/noga');
        }
        else{
            echo "Nekaj je slo narobe. Dostop zavrnjen!";
        }
     
    }

}