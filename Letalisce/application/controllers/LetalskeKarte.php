<?php
class LetalskeKarte extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('LetalskeKarteDb');

        if ($this->session->userdata('prijavljen') !== TRUE) {
            redirect('PrvaStran');
          }
    }



    public function vrni_ime()
    {

        $imenik = array(
            'ime' => $this->session->userdata('ime')
        );

        return $imenik;
    }

    function pregled()
    {

        $ime = $this->vrni_ime();

        $userdata['ime'] = $this->session->userdata('ime');
        $data = $this->LetalskeKarteDb->vrniLetKarte();
        $flashi = array(
            'uspesno' => $this->session->flashdata('uspesno'),
            'neuspesno' => $this->session->flashdata('neuspesno')

        );




        //echo '<pre>';
        //print_r($data);
        //echo '</pre>';
        //exit();


        $this->load->view('templates/glava', $ime);
        $this->load->view('letalskeK', ['data' => $data, 'flashi' => $flashi]);
        $this->load->view('templates/noga');
    }


    function novaLetKarta()
    {
        $ime = $this->vrni_ime();
        $flashi = array(
            'uspesno' => $this->session->flashdata('uspesno'),
            'neuspesno' => $this->session->flashdata('neuspesno')

        );
        //kar povlecemo najvecji id iz tabele in ga nastavimo kot value!
        $data_prej = $this->LetalskeKarteDb->vrniLetKartaId();
        $data_potem = $data_prej[0]->id + 1;

        //echo '<pre>';
        //print_r($data_potem);
        //echo '</pre>';
        //exit();

        $this->load->view('templates/glava', $ime);
        $this->load->view('dodajLetKarto', ['data' => $data_potem, 'flashi' => $flashi]);
        $this->load->view('templates/noga');
    }




    function shraniNovoLetKarto()
    {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('ime', 'Ime', 'required');
        $this->form_validation->set_rules('priimek', 'Priimek', 'required');
        $this->form_validation->set_rules('ura_leta', 'Ura_leta', 'required');
        $this->form_validation->set_rules('datum_leta', 'Datum_leta', 'required');

        $this->form_validation->set_message('required', 'Polje {field} je potrebno izpolniti!');
        $this->form_validation->set_message('regex_match', 'Pravilno izpolni polje {field}!');


        $this->form_validation->set_error_delimiters('<div id="napaka">', '</div>');
        if ($this->form_validation->run() !== FALSE) {

            //vsi podatki, razen ponovnega gesla
            $podatki = array(
                'id' => $this->input->post('id'),
                'ime' => $this->input->post('ime'),
                'priimek' => $this->input->post('priimek'),
                'destinacija' => $this->input->post('destinacija'),
                'ura_leta' => $this->input->post('ura_leta'),
                'datum_leta' => $this->input->post('datum_leta')
            );


            //echo '<pre>';
            //print_r($podatki);
            //echo '</pre>';
            //exit();

            //tukaj uporabim regex, da preverim ce je format datuma enak: 
            if (preg_match("/^(0?[1-9]|1[0-9]|2[0-9]|3[0-1])-(0?[1-9]|1[0-2])-(19[5-9][0-9]|20[0-4][0-9]|2050)$/", $podatki['datum_leta'])) {

                if ($this->LetalskeKarteDb->dodajNovoLetKarto($podatki)) {
                    $this->session->set_flashdata('uspesno', 'Letalska karta USPESNO dodana!');
                } else {
                    $this->session->set_flashdata('neuspesno', 'Letalska karta NI bila uspesno dodana!');
                }
            } else {
                $this->session->set_flashdata('neuspesno', 'Datum NI bil uspesno formatiran: pravilno: dd-mm-llll !');
                return redirect(base_url() . 'index.php/letalskeKarte/novaLetKarta');
            }

            //po spremembi brez veze, da se vrnemo na se enkrat spreminjanje
            //return redirect(base_url().'index.php/clanstvo/urediZaposlenega/'.$id);
            return redirect(base_url() . 'index.php/letalskeKarte/pregled');
        } else {


            $this->novaLetKarta();
        }
    }
    
    
    function urediLetKarto($id)
    {
        $ime = $this->vrni_ime();
        $flashi = array(
            'uspesno' => $this->session->flashdata('uspesno'),
            'neuspesno' => $this->session->flashdata('neuspesno')

        );

        $data = $this->LetalskeKarteDb->vrniLetKartoSpec($id);

            //echo '<pre>';
            //print_r($data);
            //echo '</pre>';
            //exit();

        $this->load->view('templates/glava', $ime);
        $this->load->view('urediLetKarto', ['data' => $data[0], 'flashi' => $flashi]);
        $this->load->view('templates/noga');
        
    }



    
    function shraniurejenoLetKarto($id)
    {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('ura_leta', 'Ura_leta', 'required');
        $this->form_validation->set_rules('datum_leta', 'Datum_leta', 'required');

        $this->form_validation->set_message('required', 'Polje {field} je potrebno izpolniti!');
        $this->form_validation->set_message('regex_match', 'Pravilno izpolni polje {field}!');


        $this->form_validation->set_error_delimiters('<div id="napaka">', '</div>');
        if ($this->form_validation->run() !== FALSE) {

            //vsi podatki, razen ponovnega gesla
            $podatki = array(
                'id' => $this->input->post('id'),
                'ime' => $this->input->post('ime'),
                'priimek' => $this->input->post('priimek'),
                'destinacija' => $this->input->post('destinacija'),
                'ura_leta' => $this->input->post('ura_leta'),
                'datum_leta' => $this->input->post('datum_leta')
            );


            //echo '<pre>';
            //print_r($podatki);
            //echo '</pre>';
            //exit();

            //tukaj uporabim regex, da preverim ce je format datuma enak: 
            if (preg_match("/^(0?[1-9]|1[0-9]|2[0-9]|3[0-1])-(0?[1-9]|1[0-2])-(19[5-9][0-9]|20[0-4][0-9]|2050)$/", $podatki['datum_leta'])) {

                if ($this->LetalskeKarteDb->urediLetKarto($id, $podatki)) {
                    $this->session->set_flashdata('uspesno', 'Letalska karta USPESNO urejena!');
                } else {
                    $this->session->set_flashdata('neuspesno', 'Letalska karta NI bila uspesno urejena!');
                }
            } else {
                $this->session->set_flashdata('neuspesno', 'Datum NI bil uspesno formatiran: pravilno: dd-mm-llll !');
                return redirect(base_url() . 'index.php/letalskeKarte/pregled');
            }

            //po spremembi brez veze, da se vrnemo na se enkrat spreminjanje
            //return redirect(base_url().'index.php/clanstvo/urediZaposlenega/'.$id);
            return redirect(base_url() . 'index.php/letalskeKarte/pregled');
        } else {


            $this->urediLetKarto($id);
        }
    }


    function izbrisiLetKarto($id) {
        $this->LetalskeKarteDb->izbrisiLetKarto($id);
        return redirect(base_url() . 'index.php/letalskeKarte/pregled');
    }
}
