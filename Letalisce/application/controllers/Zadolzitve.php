<?php
class Zadolzitve extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('zadolzitveDb');
        //ce zastavica prijavljen ni nastavljena na TRUE, potem ocitno nismo prijavljeni in se prijava poskusi ponovno
        if ($this->session->userdata('prijavljen') !== TRUE) {
            redirect('PrvaStran');
        }
    }




    public function vrni_ime()
    {

        $imenik = array(
            'ime' => $this->session->userdata('ime')
        );

        return $imenik;
    }


    function pregled()
    {
        $ime = $this->vrni_ime();
        $flashi = array(
            'uspesno' => $this->session->flashdata('uspesno'),
            'neuspesno' => $this->session->flashdata('neuspesno')

        );

        $data['zadolzitve'] = $this->zadolzitveDb->vrniZadolzitve();



        //obracanje INT -> TXT
        foreach ($data['zadolzitve'] as $zadolzitev) {

            if ($zadolzitev->status == 0) {
                $zadolzitev->status  = 'Ni odpravljeno';
            } else {
                $zadolzitev->status  = 'Odpravljeno';
            }
        }


        //echo '<pre>';
        //print_r($data['zadolzitve']);
        //echo '</pre>';
        //exit();

        $this->load->view('templates/glava', $ime);
        $this->load->view('zadolzitve', ['flashi' => $flashi, 'data' => $data]);
        $this->load->view('templates/noga');
    }






    function dodajzadolzitev()
    {
        $ime = $this->vrni_ime();

        $this->load->view('templates/glava', $ime);
        $this->load->view('dodajzadolzitev');
        $this->load->view('templates/noga');
    }




    function shraniNovoZadolzitev()
    {
        $ime = $this->vrni_ime();



        $this->load->library('form_validation');
        $this->form_validation->set_rules('naslov', 'Naslov', 'required');
        $this->form_validation->set_rules('vsebina', 'Vsebina', 'required');
        $this->form_validation->set_rules('oznaka', 'Oznaka', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');

        $this->form_validation->set_message('required', 'Polje {field} je potrebno izpolniti!');

        $this->form_validation->set_error_delimiters('<div id="napaka">', '</div>');
        if ($this->form_validation->run() !== FALSE) {
            //vsi podatki
            $podatki = array(
                'naslov' => $this->input->post('naslov'),
                'vsebina' => $this->input->post('vsebina'),
                'oznaka' => $this->input->post('oznaka'),
                'status' => $this->input->post('status')
            );

            //echo '<pre>';
            //print_r($podatki);
            //echo '</pre>';
            //exit();


            if ($podatki['status'] == 'Ni odpravljeno') {
                $podatki['status'] = 0;
            } else {
                $podatki['status'] = 1;
            }

            if ($this->zadolzitveDb->dodaj_zadolzitev($podatki)) {
                $this->session->set_flashdata('uspesno', 'Zadolzitev USPESNO dodana!');
            } else {
                $this->session->set_flashdata('neuspesno', 'Zadolzitev NI bila uspesno dodana!');
            }

            //echo '<pre>';
            //print_r($podatki);
            //echo '</pre>';
            //exit();


            //po spremembi brez veze, da se vrnemo na se enkrat spreminjanje
            //return redirect(base_url().'index.php/clanstvo/urediZaposlenega/'.$id);
            return redirect(base_url() . 'index.php/zadolzitve/pregled');
        } else {

            $this->dodajZadolzitev();
        }
    }




    function urediZadolzitev($naslov)
    {
        $ime = $this->vrni_ime();
        $nas = $naslov;
        $flashi = array(
            'uspesno' => $this->session->flashdata('uspesno'),
            'neuspesno' => $this->session->flashdata('neuspesno')

        );

        $data['urejena'] = $this->zadolzitveDb->vrniSpecificnoZadolzitev($naslov);
        $sciscena = $data['urejena'][0];

        //echo '<pre>';
        //print_r($data['urejena'][0]);
        //echo '</pre>';
        //exit();

        if ($sciscena->status == 0) {
            $sciscena->status = 'Ni odpravljeno';
        } else {
            $sciscena->status = 'Odpravljeno';
        }

        $this->load->view('templates/glava', $ime);
        $this->load->view('urediZadolzitev', ['flashi' => $flashi, 'data' => $sciscena]);
        $this->load->view('templates/noga');
    }



    function shraniSpremembeZadolzitve($naslov)
    {
        $ime = $this->vrni_ime();
        //vsi podatki
        $podatki = array(
            'naslov' => $this->input->post('naslov'),
            'vsebina' => $this->input->post('vsebina'),
            'oznaka' => $this->input->post('oznaka'),
            'status' => $this->input->post('status')
        );

        //echo '<pre>';
        //print_r($podatki);
        //echo '</pre>';
        //exit();


        if ($podatki['status'] == 'Ni odpravljeno') {
            $podatki['status'] = 0;
        } else {
            $podatki['status'] = 1;
        }

        if ($this->zadolzitveDb->spremeni_zadolzitev($podatki, $naslov)) {
            $this->session->set_flashdata('uspesno', 'Zadolzitev USPESNO spremenjena!');
        } else {
            $this->session->set_flashdata('neuspesno', 'Zadolzitev NI bila uspesno spremenjena!');
        }

        //echo '<pre>';
        //print_r($podatki);
        //echo '</pre>';
        //exit();


        //po spremembi brez veze, da se vrnemo na se enkrat spreminjanje
        //return redirect(base_url().'index.php/clanstvo/urediZaposlenega/'.$id);
        return redirect(base_url() . 'index.php/zadolzitve/pregled');
    }

    function izbrisiZadolzitev($naslov)
    {
        $this->zadolzitveDb->odstraniZadolzitev($naslov);
        redirect('zadolzitve/pregled');
    }
}
